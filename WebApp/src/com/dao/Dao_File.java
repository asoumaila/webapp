package com.dao;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class Dao_File {

	MongoDb m = new MongoDb();
	DBCollection coll;

	public Dao_File() {
		coll = MongoDb.getCollection("File_");
	}

	public String save(File_ infile) {
		m.ds.save(infile);
		return infile.getId_file().toString();
	}

	public File_ findById(String id) {

		return m.ds.get(File_.class, new ObjectId(id));
	}

	public void remove(String id) {
		if (id != null) {

			BasicDBObject fileEntryDbObj = (BasicDBObject) coll
					.findOne(new BasicDBObject("_id", new ObjectId(id)));
			coll.remove(fileEntryDbObj);
		} else {
			coll.remove(new BasicDBObject());
		}

	}

	public List<File_> findByName(String name) {

		return m.ds.createQuery(File_.class).field("name").equal(name).asList();
	}

	public List<File_> findBydate(long date) {

		return m.ds.createQuery(File_.class).field("entry_Date").equal(date)
				.asList();
	}

	public void updatefile(File_ old, File_ n) {
		coll.update(m.morphia.toDBObject(old), m.morphia.toDBObject(n));
	}

	public List<File_> readfile(File_ p) {
		DBCursor cursor = null;
		ArrayList<File_> list = new ArrayList<File_>();

		if (p != null)
			list.add(findById(p.getId_file()));
		else
			cursor = coll.find();
		do {
			list.add(m.morphia.fromDBObject(File_.class, cursor.next()));
		} while (cursor.hasNext());

		return list;
	}

	/**
	 * m�thode retournant le fichier � signer par un signataire identifi� par
	 * id_profile
	 * 
	 * @param id_profile
	 * @return
	 */
	public List<File_> findFilesOfProfil(Profil profil) {

		// DBObject query = new BasicDBObject();

		// Profil profil = m.ds.createQuery(Profil.class).field("id_ldap")
		// .equal(profil).asList().get(0);
		// query.put("id_signer", profil.getId_profil());

		return m.ds.createQuery(File_.class).field("id_signer")
				.equal(profil.getId_profil()).asList();

	}

	public List<File_> findunsignedfiles(String idldap) {

		return m.ds.createQuery(File_.class).field("id_signer").equal(idldap)
				.field("signed").equal(0).asList();

	}
}
