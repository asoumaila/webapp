package com.dao;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class Dao_profil {

	MongoDb m = new MongoDb();
	DBCollection coll;

	public Dao_profil() {
		coll = MongoDb.getCollection("Profil");
	}

	public String save(Profil inprofil) {
		m.ds.save(inprofil);
		return inprofil.getId_profil();
	}

	public Profil findById(String id) {

		return m.ds.get(Profil.class, new ObjectId(id));
	}

	public List<Profil> findByIdldap(String idldap) {
		List<Profil> result = m.ds.createQuery(Profil.class).field("id_ldap")
				.equal(idldap).asList();
		return result;
	}

	public void remove(String id) {
		if (id != null) {

			BasicDBObject profilEntryDbObj = (BasicDBObject) coll
					.findOne(new BasicDBObject("_id", new ObjectId(id)));
			coll.remove(profilEntryDbObj);
		} else {
			coll.remove(new BasicDBObject());
		}

	}

	public void updateprofil(Profil old, Profil n) {
		coll.update(m.morphia.toDBObject(old), m.morphia.toDBObject(n));
	}

	public List<Profil> readprofile(String p) {
		DBCursor cursor = null;
		ArrayList<Profil> list = new ArrayList<Profil>();

		if (p != null)
			list.add(findById(p));
		else {
			cursor = coll.find();
			do {
				list.add(m.morphia.fromDBObject(Profil.class, cursor.next()));
			} while (cursor.hasNext());
		}

		return list;
	}

	public List<Profil> findByIdMobile(String idMobile) {
		List<Profil> result = m.ds.createQuery(Profil.class).field("id_mobile")
				.equal(idMobile).asList();
		return result;
	}

}