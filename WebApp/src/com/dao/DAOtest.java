package com.dao;

import static org.junit.Assert.assertEquals;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.junit.Test;

public class DAOtest {

	@Test
	public void testAddProfil() {

		Dao_profil dao = new Dao_profil();

		dao.remove(null);
		Profil p = new Profil();
		p.setId_ldap("id_ldap");
		p.setId_mobile("id_mobile");
		// p.setUrl("url");
		p.setCertifpasswd("password");

		// chargement du certificat a partir d'un fichier
		byte[] buffer = null;
		try {
			File certifFile = new File("myCertificate.pfx");
			buffer = new byte[(int) certifFile.length()];
			DataInputStream in = new DataInputStream(new FileInputStream(
					certifFile));
			in.readFully(buffer);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		p.setCertif(buffer);

		String s = dao.save(p);
		System.out.println(s);
		System.out.println("\n ***** all ***** \n ");
		printfileList(dao.readprofile(null));

	}

	private void printfileList(List<Profil> list) {

		for (Profil p : list) {
			System.out.print(p.getId_profil() + " " + p.getId_ldap() + " "
					+ p.getId_mobile() + " " + " " + p.getCertif() + " "
					+ p.getCertifpasswd());
			System.out.println();
		}
	}

	@Test
	public void testAddFile() {
		Dao_File dao = new Dao_File();

		dao.remove(null);
		File_ f = new File_();
		f.setEntry_Date(03012013);
		f.setId_Workflow(null);
		f.setName("regle.pdf");
		f.setPath("C:\\Users\\phenix\\Desktop\\signature");

		Dao_profil daoProfil = new Dao_profil();
		Profil profil = null;
		profil = daoProfil.readprofile(null).get(0);

		f.setId_signer(profil.getId_profil());

		String s = dao.save(f);
		List<File_> fich = dao.findFilesOfProfil(profil);
		// System.out.println(fich);
		assertEquals(f.getPath(), fich.get(0).getPath());
	}

}
