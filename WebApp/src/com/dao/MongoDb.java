package com.dao;

import java.net.UnknownHostException;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class MongoDb {
	public Morphia morphia = new Morphia();
	@SuppressWarnings("deprecation")
	public Datastore ds = morphia.createDatastore(dbName);

	private static String host = "localhost";
	private static int port = 27017;
	static Mongo mongo;

	private static DB db;
	private static String dbName = "appdb";

	public MongoDb() {

		morphia.map(File_.class).map(Profil.class);
	}

	public static DBCollection getCollection(String collectionName) {
		if (mongo == null) {
			try {
				mongo = new Mongo(host, port);
			} catch (UnknownHostException e) {

				e.printStackTrace();
			} catch (MongoException e) {

				e.printStackTrace();
			}
		}
		db = mongo.getDB(dbName);

		return db.getCollection(collectionName);
	}

	public static void setHost(String h) {
		host = h;
	}

	public static void setPort(int p) {
		port = p;
	}
}
