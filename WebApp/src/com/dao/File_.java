package com.dao;

import javax.xml.bind.annotation.XmlRootElement;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@XmlRootElement
@Entity
public class File_ {

	@Id
	private String id_file;
	private String path;
	private String name;
	private String id_Workflow;
	private long entry_Date;
	private String id_signer;
	private long file_size;
	private int signed = 0;

	public long getFile_size() {
		return file_size;
	}

	public void setFile_size(long file_size) {
		this.file_size = file_size;
	}

	public String getId_file() {
		return id_file;
	}

	public void setId_file(String id_file) {
		this.id_file = id_file;
	}

	public String getPath() {
		return path;
	}

	public String getId_Workflow() {
		return id_Workflow;
	}

	public void setId_Workflow(String id_Workflow) {
		this.id_Workflow = id_Workflow;
	}

	public String getId_signer() {
		return id_signer;
	}

	public void setId_signer(String id_signer) {
		this.id_signer = id_signer;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getEntry_Date() {
		return entry_Date;
	}

	public void setEntry_Date(long entry_Date) {
		this.entry_Date = entry_Date;
	}

	public int getSigned() {
		return signed;
	}

	public void setSigned(int signed) {
		this.signed = signed;
	}

	@Override
	public String toString() {

		return this.path + "  " + this.id_signer;
	}
}
