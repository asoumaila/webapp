package com.dao;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;

public class Main {

	static Dao_profil daoPro = new Dao_profil();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AddProfil(
				"ali",
				"APA91bEniWeFXSVN3HK-AFTaTX90EY74rDz-nN02qmBpaF5UPbyrKzt28KSVma36N4XBdo_Yb-qCaAgwv-cgTgu5TBC_rc-weWXMDUII3sVjxXhlPJHtTbxO8vqEpp2VFhF16c5QMcS88Yzz4q7c5t5mJunv_wClpQ");

	}

	public static void AddProfil(String idLdap, String idMobile) {
		Profil p = new Profil();
		p.setId_ldap(idLdap);
		p.setId_mobile(idMobile);
		// p.setUrl("url");
		p.setCertifpasswd("password");

		// chargement du certificat a partir d'un fichier
		byte[] buffer = null;
		try {
			File certifFile = new File("myCertificate.pfx");
			buffer = new byte[(int) certifFile.length()];
			DataInputStream in = new DataInputStream(new FileInputStream(
					certifFile));
			in.readFully(buffer);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		p.setCertif(buffer);
		daoPro.save(p);

	}

	public static void removeProfil(String id_signer) {

		Profil profil = daoPro.findByIdldap(id_signer).get(0);
		if (profil != null) {

			daoPro.remove(profil.getId_profil());
		}

	}

}
