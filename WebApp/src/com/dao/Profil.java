package com.dao;

import java.util.Arrays;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@Entity
public class Profil {
	@Id
	private String id_profil;

	// private String name;
	// private String url;
	private String id_mobile;
	private String id_ldap;
	private String certifpasswd;
	private byte[] certif;
	private int registed = 0;

	public int getRegisted() {
		return registed;
	}

	public void setRegisted(int registed) {
		this.registed = registed;
	}

	public String getId_profil() {
		return id_profil;
	}

	public String getId_mobile() {
		return id_mobile;
	}

	public String getId_ldap() {
		return id_ldap;
	}

	public byte[] getCertif() {
		return certif;
	}

	public void setId_profil(String id_profil) {
		this.id_profil = id_profil;
	}

	public void setId_mobile(String id_mobile) {
		this.id_mobile = id_mobile;
	}

	public void setId_ldap(String id_ldap) {
		this.id_ldap = id_ldap;
	}

	public void setCertif(byte[] certif) {
		this.certif = certif;
	}

	public String getCertifpasswd() {
		return certifpasswd;
	}

	public void setCertifpasswd(String certifpasswd) {
		this.certifpasswd = certifpasswd;
	}

	public void setProfil(Profil pro) {
		this.setCertif(pro.getCertif());
		this.setCertifpasswd(pro.getCertifpasswd());
		this.setId_ldap(pro.getId_ldap());
		this.setId_mobile(pro.getId_mobile());
		this.setId_profil(pro.getId_profil());
		this.setRegisted(pro.getRegisted());

	}

	@Override
	public String toString() {
		return "Profil [id_profil=" + id_profil + ", id_mobile=" + id_mobile
				+ ", id_ldap=" + id_ldap + ", certifpasswd=" + certifpasswd
				+ ", certif=" + Arrays.toString(certif) + ", registed="
				+ registed + "]";
	}

}
