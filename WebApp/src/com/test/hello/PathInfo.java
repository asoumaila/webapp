package com.test.hello;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 
 * classe permettant de manipuler le fichier de configuration
 * (/ressources/props.xml)
 * 
 */

public class PathInfo {

	private static volatile PathInfo instance = null;
	private final Properties props;

	public static PathInfo getInstance() {
		if (instance == null) {
			synchronized (PathInfo.class) {
				instance = new PathInfo();
			}
		}
		return instance;
	}

	public Properties getProperties() {
		return props;
	}

	public PathInfo() {
		props = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(new File(
					props.getProperty("ressources/props.xml")));
			props.loadFromXML(input);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}
}
