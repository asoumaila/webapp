package com.test.hello;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import com.dao.Dao_File;
import com.dao.Dao_profil;
import com.dao.File_;
import com.dao.Profil;
import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

public class ActionSign {
	private Dao_profil dao;
	private Dao_File daoFile;

	private Message createMessage(String messageTosend) {

		Message message = new Message.Builder().addData("message",
				messageTosend).build();
		return message;

	}

	protected Sender newSender(String ApiKey) {
		if (ApiKey == null || ApiKey.trim().length() == 0) {
			throw new NullPointerException();
		}
		return new Sender(ApiKey);
	}

	protected boolean isEmptyOrNull(String value) {
		return value == null || value.trim().length() == 0;
	}

	protected void saveFileToSign(String fileName, long fileSize,
			String repPath, String id_workflow, long enty_date, String id_signer) {
		daoFile = new Dao_File();
		File_ rcvfile = new File_();
		rcvfile.setName(fileName);
		rcvfile.setEntry_Date(enty_date);
		rcvfile.setFile_size(fileSize);
		rcvfile.setPath(repPath);
		rcvfile.setId_signer(id_signer);
		rcvfile.setId_Workflow(id_workflow);

		daoFile.save(rcvfile);
	}

	protected boolean sendSignatureRequest(Sender sender, Profil profil,
			String message) {

		boolean send = false;
		dao = new Dao_profil();

		Message mess = createMessage(message);
		Result result = null;

		try {
			if (profil.getRegisted() == 1) {
				result = sender.sendNoRetry(mess, profil.getId_mobile());
			} else {

				return send;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (result.getMessageId() != null) {
			send = true;
			// successfully send to device
			String canonicalId = result.getCanonicalRegistrationId();
			if (canonicalId != null) {
				// the device RegID has changed update it
				Profil pro = new Profil();
				pro = profil;
				pro.setId_mobile(canonicalId);
				dao.updateprofil(profil, pro);
			}

		} else {
			String error = result.getErrorCodeName();
			if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
				// application supprim� du device
				dao.remove(profil.getId_profil());
				List<File_> files = daoFile.findFilesOfProfil(profil);

				for (File_ file : files) {
					daoFile.remove(file.getId_file());

				}

			} else {

				// erreur de l'envoi du message
				System.out.println(error);
			}

		}

		return send;

	}

	// save uploaded file to new location
	protected void writeToFile(InputStream uploadedInputStream,
			String uploadedFileLocation) {
		OutputStream out = null;

		try {
			out = new FileOutputStream(new File(uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];

			out = new FileOutputStream(new File(uploadedFileLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}

		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			if (out != null) {
				try {

					// out.flush();
					out.close();

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (uploadedInputStream != null) {
				try {
					uploadedInputStream.close();

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

}
