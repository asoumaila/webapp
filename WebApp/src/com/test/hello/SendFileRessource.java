package com.test.hello;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuxeo.ecm.automation.client.Constants;
import org.nuxeo.ecm.automation.client.Session;
import org.nuxeo.ecm.automation.client.jaxrs.impl.HttpAutomationClient;
import org.nuxeo.ecm.automation.client.model.FileBlob;

import signature.SignatureBC;

import com.dao.Dao_File;
import com.dao.File_;

@Path("/sign")
public class SendFileRessource extends SignFile {
	// Properties props = PathInfo.getInstance().getProperties();

	/**
	 * 
	 * @param url
	 *            : represente le chemin du fichier mais pas necessaire car l'id
	 *            permet de le retrouver dans la BD
	 * @param id
	 * @return
	 */

	@Path("/file")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@POST
	public Response SenfileToUrl(@FormParam("idFile") String id) {

		Dao_File daofile = new Dao_File();
		File_ file = new File_();
		file = daofile.findById(id);

		if (isEmptyOrNull(id) || file == null) {
			return Response.status(400).build();
		}

		SignatureBC.SignBC(id);

		HttpAutomationClient client = new HttpAutomationClient(
				"http://82.244.83.215:9000/nuxeo/site/automation");

		Session session = client.getSession("Administrator", "Administrator");

		FileBlob fb = new FileBlob(new File(getFilesFromRep(file.getPath())
				.get("signature")));

		fb.setMimeType("application/PK7-Factura");
		// uploading a file will return null since we used HEADER_NX_VOIDOP

		try {
			session.newRequest("Blob.Attach")
					.setHeader(Constants.HEADER_NX_VOIDOP, "true").setInput(fb)
					.set("document", file.getId_Workflow())
					.set("xpath", "files:files").execute();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		client.shutdown();
		// HttpClient client = new DefaultHttpClient();
		// System.out.println(props.getProperty("urlNuxeo").trim());
		// HttpPost post = new HttpPost(props.getProperty("urlNuxeo").trim());
		//
		// // les chemins des fichiers � recuperer au niveau de la base de
		//
		// Map<String, String> pathFiles = getFilesFromRep(file.getPath());
		//
		// // rep.listFiles()[0].getName().split("\\.")
		// FileBody file1 = new FileBody(new File(pathFiles.get("file")));
		// FileBody file2 = new FileBody(new File(pathFiles.get("signature")));
		//
		// MultipartEntity requestEntity = new MultipartEntity();
		// requestEntity.addPart("lefichier", file1);
		// requestEntity.addPart("signature", file2);
		// post.setEntity(requestEntity);
		//
		// try {
		// client.execute(post);
		// } catch (ClientProtocolException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		return Response.ok("tout va bien").build();
	}

	public Map<String, String> getFilesFromRep(String rep) {
		File rep_sign = new File(rep);
		File file[] = rep_sign.listFiles();
		String filename = "";
		Map<String, String> pathFichier = new HashMap<String, String>();

		for (int i = 0; i < file.length; i++) {
			filename = file[i].getName();

			if (filename.substring(filename.lastIndexOf(".") + 1).equals("pk7")) {
				pathFichier.put("signature", file[i].getAbsolutePath());
			} else {
				pathFichier.put("file", file[i].getAbsolutePath());
			}

		}
		return pathFichier;
	}
}
