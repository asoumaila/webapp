package com.test.hello;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.dao.Dao_File;
import com.dao.Dao_profil;
import com.dao.Profil;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

/**
 * 
 * @author Soumaila Ali,Ayoub,Ilal
 * 
 */

@Path("/file")
public class UploadFileService extends ActionSign {
	private Sender sender;
	// Properties props = PathInfo.getInstance().getProperties();
	private final static String API_KEY = "AIzaSyDEGQuvitAMnR0JwIkVaaRPo0LddD0BI90";

	/**
	 * 
	 * @param uploadedInputStream
	 *            :le flux envoy� par le client JSP
	 * @param fileDetail
	 *            :la metadonn�e
	 * @param id
	 *            :id du signataire
	 * @param user_name
	 *            :le nom du signataire
	 * @return
	 */

	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
			@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail,
			@FormDataParam("id_user") String idSigner,
			@FormDataParam("url_nuxeo") String fileLocationInNuxeo) {
		String output = "";
		if (isEmptyOrNull(idSigner) || isEmptyOrNull(fileLocationInNuxeo)
				|| isEmptyOrNull(fileDetail.getFileName())) {
			return Response.status(400).build();

		}

		String filename = fileDetail.getFileName();

		System.out.println(filename);
		System.out.println(System.getProperty("user.home"));
		File data = new File(System.getProperty("user.home") + "//filedepot");
		if (!data.exists()) {
			data.mkdir();
		}
		File rep = new File(data.getAbsolutePath() + "//" + filename
				+ System.currentTimeMillis());
		rep.mkdir();
		String repPath = rep.getAbsolutePath();
		String uploadedFileLocation = repPath + "//" + filename;

		Dao_profil daoprofil = new Dao_profil();
		Profil profil = new Profil();
		//
		List<Profil> profils = daoprofil.findByIdldap(idSigner);
		if (profils.isEmpty()) {
			// Le signataire n'a pas encore d'identifiant mobile (pas encore
			// enregistr� depuis son mobile)
			// on peut l'ajouter dans la base de donn�es et attendre qu'il est
			// son
			// identifiant mobile pour pouvoir signer les fichiers et dans ce
			// cas
			// on doit pouvoir recuperer tous les champs n�cessaires � la
			// creation d'un
			// certificat depuis nuxeo
			// pour ce sprint on traite ce cas
			return Response.status(401).build();

		} else {
			// save the file in the location repPath
			writeToFile(uploadedInputStream, uploadedFileLocation);
			long fileSize = new File(uploadedFileLocation).length();
			System.out.println(fileSize);
			profil = profils.get(0);
			// envoi de la requete de signature
			sender = newSender(API_KEY);
			if (sendSignatureRequest(sender, profil,
					"vous avez un fichier a signer")) {

				saveFileToSign(filename, fileSize, repPath,
						fileLocationInNuxeo, System.currentTimeMillis(),
						profil.getId_profil());
				output = "Push work successfull";
			} else {
				rep.delete();
				output = "Push does not work";

			}
		}
		ResponseBuilder response = Response.ok(output);// new
														// File("D://uploaded/sign.pdf"));
		// response.header("Content-Disposition",
		// "attachment; filename=bonj.pdf");
		return response.build();

	}

	@GET
	@Path("{idldap}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFiles(@PathParam("idldap") String idldap) {
		String files = "";

		Dao_File daoFile = new Dao_File();
		Dao_profil daoprofil = new Dao_profil();
		List<Profil> profils = daoprofil.findByIdldap(idldap);
		if (!profils.isEmpty()) {
			Gson gson = new Gson();

			files = gson.toJson(daoFile.findunsignedfiles(profils.get(0)
					.getId_profil()));
		}
		return files;

	}
}
