package com.test.hello;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import signature.Certificat;

import com.dao.Dao_profil;
import com.dao.Profil;

@Path("/user")
public class SaveUser {
	// Properties prop = PathInfo.getInstance().getProperties();
	Dao_profil dao = new Dao_profil();

	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response register(@FormParam("idNuxeo") String id_signataire,
			@FormParam("regId") String id_mobile,
			@FormParam("name") String username,
			@FormParam("organisationName") String organisationName,
			@FormParam("organisationUnit") String organisationUnit,
			@FormParam("localityName") String localityName,
			@FormParam("stateName") String statename,
			@FormParam("country") String country,
			@FormParam("password") String password) {
		Response response = null;
		if (validation(id_signataire, id_mobile, username, organisationName,
				organisationUnit, localityName, statename, country, password)) {
			// veuillez renseigner tous les champs;
			response = Response.status(422).build();

		} else {
			Profil profil = new Profil();
			// why return a list
			List<Profil> profils = dao.findByIdldap(id_signataire);
			if (profils.isEmpty()) {
				profil.setId_ldap(id_signataire);
				profil.setId_mobile(id_mobile);
				// profil.setUrl("thsaksak");
				profil.setRegisted(1);
				profil.setCertifpasswd(password);
				try {

					byte[] certif = Certificat.createcertificate(username,
							organisationUnit, organisationName, localityName,
							statename, country, 365, password);
					profil.setCertif(certif);
				} catch (IOException e) { // TODO Auto-generated catch block
					e.printStackTrace();

				} catch (Exception e) { // TODO Auto-generatedcatch block
					e.printStackTrace();
				}
				dao.save(profil);

			} else {
				profil = profils.get(0);
				Profil pro = new Profil();
				pro.setProfil(profil);
				pro.setId_mobile(id_mobile);
				dao.updateprofil(profil, pro);
			}
			response = Response.status(200).build();

		}
		return response;
	}

	@Path("/unregister")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response unregister(@FormParam("regId") String regId) {
		List<Profil> profils;
		Response response = null;
		if (isEmptyOrNull(regId)) {
			response = Response.status(422).build();
		} else {
			profils = dao.findByIdMobile(regId);
			if (!profils.isEmpty()) {
				Profil profil = profils.get(0);
				Profil pro = new Profil();
				pro.setProfil(profil);
				pro.setRegisted(0);
				System.out.println(pro.toString());
				System.out.println(profil.toString());
				dao.updateprofil(profil, pro);
				response = Response.status(200).build();

			} else {
				response = Response.status(202).build();
			}

		}
		return response;
	}

	private boolean validation(String id_signataire, String id_mobile,
			String username, String organisationName, String organisationUnit,
			String localityName, String stateName, String country,
			String password) {

		return (isEmptyOrNull(id_signataire) || isEmptyOrNull(id_mobile)
				|| isEmptyOrNull(username) || isEmptyOrNull(organisationName)
				|| isEmptyOrNull(organisationUnit)
				|| isEmptyOrNull(localityName) || isEmptyOrNull(stateName)
				|| isEmptyOrNull(country) || isEmptyOrNull(password));
	}

	private boolean isEmptyOrNull(String value) {

		return value == null || value.trim().length() == 0;

	}

}
