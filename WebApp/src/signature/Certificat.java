package signature;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 
 * la classe permettant de g�n�rer un certificat
 * 
 */
public class Certificat {

	static String keystorepath = System.getProperty("user.home");
	static String keytoolpath = System.getProperty("java.home")
			+ System.getProperty("file.separator") + "bin"
			+ System.getProperty("file.separator") + "keytool";

	public static byte[] createcertificate(String username,
			String organizationUnit, String organizationName,
			String localityName, String stateName, String country,
			int validity, String password) throws IOException,
			InterruptedException {

		String outputfile = keystorepath + "//" + username + ".p12";

		String dnameoptions = "\"CN=" + username + ", OU=" + organizationUnit
				+ ", O=" + organizationName + ", L=" + localityName + ", S="
				+ stateName + ", C=" + country + "\" ";
		String sGen = keytoolpath + " -genkey -rfc" + " -alias " + username
				+ " -dname " + dnameoptions + " -keyalg RSA" + " -validity "
				+ validity + " -keysize " + 1024 + " -keypass " + password
				+ " -storetype pkcs12" + " -keystore " + keystorepath + "/"
				+ username + ".p12" + " -storepass " + password;

		if (System.getProperty("os.name").startsWith("Win")) {

			Process gen = Runtime.getRuntime().exec(sGen);
			gen.waitFor();

		} else {

			File tempcmd = File.createTempFile("commandy", "sh");
			FileOutputStream fos = new FileOutputStream(tempcmd);
			String bash = "#!/bin/bash \n" + sGen;
			System.out.println(bash);
			byte[] b = bash.getBytes();
			fos.write(b);
			fos.close();
			final Process pr = Runtime.getRuntime().exec(
					"sh " + tempcmd.getAbsolutePath());
			pr.waitFor();
			tempcmd.delete();
		}
		// chargement du certificat a partir d'un fichier
		byte[] buffer = null;
		try {
			File certifFile = new File(outputfile);
			buffer = new byte[(int) certifFile.length()];
			DataInputStream in = new DataInputStream(new FileInputStream(
					certifFile));
			in.readFully(buffer);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return buffer;

	}

}
