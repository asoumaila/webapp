package signature;

import java.io.*;
import java.util.*;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CRLSelector;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.X509Certificate;

import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.DefaultCMSSignatureAlgorithmNameGenerator;
import org.bouncycastle.cms.SignerInformation;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentVerifierProvider;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.operator.bc.BcRSAContentVerifierProviderBuilder;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.bouncycastle.util.Store;

import com.itextpdf.text.log.SysoLogger;
import java.security.cert.CollectionCertStoreParameters;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.bc.BcRSASignerInfoVerifierBuilder;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;

public class VerificationBC {

	static String Sname  = "C:\\Users\\phenix\\Desktop\\signature\\signature.pk7";
	static String Oname  = "C:\\Users\\phenix\\Desktop\\signature\\HelloWorld_sign.pdf";
	static String certificat  = "C:\\Users\\phenix\\Desktop\\signature\\myCertificate.pfx";
	
	public static final void verify() {
		try {
			
			// CHARGEMENT DU FICHIER PKCS#12
			KeyStore ks = null;
			char[] password = null;
			try {
				ks = KeyStore.getInstance("PKCS12");
				// Password pour le fichier certificat
				password = "password".toCharArray();
				ks.load(new FileInputStream(certificat), password);
			} catch (Exception e) {
				System.out.println("Erreur: fichier " +
						"markSmith.p12" +
						" n'est pas un fichier pkcs#12 valide ou passphrase incorrect");
			}
				X509Certificate cert = null;
				PublicKey publickey = null;
					Enumeration en = ks.aliases();
					String ALIAS = "";
					Vector vectaliases = new Vector();

					while (en.hasMoreElements())
						vectaliases.add(en.nextElement());
					String[] aliases = (String []) (vectaliases.toArray(new String[0]));
					for (int i = 0; i < aliases.length; i++)
						if (ks.isKeyEntry(aliases[i]))
						{
							ALIAS = aliases[i];
							break;
						}
					cert = (X509Certificate)ks.getCertificate(ALIAS);
					publickey = ks.getCertificate(ALIAS).getPublicKey();
					
					X509CertificateHolder sigCert = new JcaX509CertificateHolder(cert);
					
			// Chargement du fichier original
			File f1 = new File(Oname);
			byte[] OrBuffer = new byte[(int)f1.length()];
			DataInputStream in1 = new DataInputStream(new FileInputStream(f1));
			in1.readFully(OrBuffer);
			in1.close();
			// Chargement du fichier sign�
			File f = new File(Sname);
			byte[] buffer = new byte[(int)f.length()];
			DataInputStream in = new DataInputStream(new FileInputStream(f));
			in.readFully(buffer);
			in.close();

			CMSSignedData signature = new CMSSignedData(new CMSProcessableByteArray(OrBuffer),buffer);
			SignerInformation signer = (SignerInformation)signature.getSignerInfos().getSigners().iterator().next();
			Store cs = signature.getCertificates();
			Iterator iter = cs.getMatches(signer.getSID()).iterator();
			X509CertificateHolder certificateHolder = (X509CertificateHolder) iter.next();

			// Verifie la signature
			
			//System.out.println(signer.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC").build(certificate)));
			ContentVerifierProvider contentVerifierProvider = new BcRSAContentVerifierProviderBuilder(
			        new DefaultDigestAlgorithmIdentifierFinder())
			      .build(sigCert);
			//ContentVerifierProvider contentVerifierProvider = new JcaContentVerifierProviderBuilder().setProvider("BC").build(certificate);
			// verifie si c'est le meme certificat utilis� lors de la signature
			if (!certificateHolder.isSignatureValid(contentVerifierProvider))
		    {
		        System.err.println("signature invalid");
		    }
			else System.err.println("signature is valid");
			//if (signer.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC").build(certificate)))
			// verifie si le doc n'a pas �t� modifi� depuis la signature
			if(signer.verify(new BcRSASignerInfoVerifierBuilder( new DefaultCMSSignatureAlgorithmNameGenerator(),new DefaultSignatureAlgorithmIdentifierFinder() , new DefaultDigestAlgorithmIdentifierFinder(), new BcDigestCalculatorProvider()).build(certificateHolder)))
			{
				System.err.println("signer verified");
			}
			else System.err.println("signer is not verified");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
