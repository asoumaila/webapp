package signature;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;

import com.dao.Dao_File;
import com.dao.Dao_profil;
import com.dao.File_;
import com.dao.Profil;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfSignatureAppearance;
import com.lowagie.text.pdf.PdfStamper;

public class SignaturePDF {

	// static String fname = "C:\\Users\\phenix\\Desktop\\signature\\regle.pdf";
	// static String certificat =
	// "C:\\Users\\phenix\\Desktop\\signature\\myCertificate.pfx";
	// static String password = "password";

	// static String fnameS =
	// "C:\\Users\\phenix\\Desktop\\signature\\regle_sign.pdf" ;
	// static String fnameR = "C:\\Users\\phenix\\Desktop\\signature\\regled";

	public static final void signPDF(String idFile) throws IOException, DocumentException {
		// recuperation des infos sur le fichier
		
		Dao_File DaoFile = new Dao_File();
		File_ myFile = DaoFile.findById(idFile);
		String myFileName = myFile.getName();
		String repPath = myFile.getPath();
		String idSigner = myFile.getId_signer();
		
		// recuperation du certificat du signataire
		
		Dao_profil DaoProfil = new Dao_profil();
		Profil myProfil = DaoProfil.findById(idSigner);
		String passCert = myProfil.getCertifpasswd();
		char[] password = passCert.toCharArray();
		byte[] certificat = myProfil.getCertif();

		try {
			// Creation d'un KeyStore
			KeyStore ks = KeyStore.getInstance("pkcs12");
			// Chargement du certificat p12 dans le magasin
			ks.load(new ByteArrayInputStream(certificat), password);
			String alias = ks.aliases().nextElement();
			// Recup�ration de la clef priv�e
			PrivateKey key = (PrivateKey) ks.getKey(alias, password);
			// et de la chaine de certificats
			Certificate[] chain = ks.getCertificateChain(alias);

			// Lecture du document source
			PdfReader pdfReader = new PdfReader((new File(repPath+"//"+myFileName)).getAbsolutePath());
			File outputFile = new File(repPath+"//Signed"+myFileName);
			// Creation du tampon de signature
			PdfStamper pdfStamper;
			pdfStamper = PdfStamper.createSignature(pdfReader, null, '\0', outputFile);
			PdfSignatureAppearance sap = pdfStamper.getSignatureAppearance();
			sap.setCrypto(key, chain, null, PdfSignatureAppearance.WINCER_SIGNED);
			sap.setReason("signature");
			sap.setLocation("");
			// Position du tampon sur la page (ici en bas a gauche page 1)
			sap.setVisibleSignature(new Rectangle(10, 10, 50, 30), 1, "signature");

			pdfStamper.setFormFlattening(true);
			pdfStamper.close();
			/*
			// mise � jour du nom du fichier
			File_ Newfile = new File_();
			Newfile.setName(repPath+"//Signed"+myFileName);
			//Newfile.setPath(repPath);
			//Newfile.setId_signer(idSigner);
			
			DaoFile.updatefile(myFile, Newfile);
			*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cr�ation d'un simple document PDF "Hello World"
	 * 
	 * public static void buildPDF() {
	 * 
	 * // Creation du document Document document = new Document();
	 * 
	 * try { // Creation du "writer" vers le doc // directement vers un fichier
	 * PdfWriter.getInstance(document, new FileOutputStream(fname)); //
	 * Ouverture du document document.open();
	 * 
	 * // Ecriture des datas document.add(new Paragraph("Hello World"));
	 * 
	 * } catch (DocumentException de) { System.err.println(de.getMessage()); }
	 * catch (IOException ioe) { System.err.println(ioe.getMessage()); }
	 * 
	 * // Fermeture du document document.close();
	 * 
	 * }
	 */

}