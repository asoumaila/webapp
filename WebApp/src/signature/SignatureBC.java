package signature;

/**
 * la classe qui effectue la signature et genere un fichier detach� contenant la signature
 * 
 */

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.SignerInfoGeneratorBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.util.Store;

import com.dao.Dao_File;
import com.dao.Dao_profil;
import com.dao.File_;
import com.dao.Profil;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class SignatureBC {

	public static final void SignBC(String idFile) {

		// recuperation des infos sur le fichier

		Dao_File DaoFile = new Dao_File();
		File_ myFile = DaoFile.findById(idFile);
		String myFileName = myFile.getName();
		String repPath = myFile.getPath();
		String idSigner = myFile.getId_signer();

		// ajouter d'une etiquette s'il s'agit d'un PDF

		PreSignature(myFileName, repPath);

		// recuperation du certificat du signataire

		Dao_profil DaoProfil = new Dao_profil();
		Profil myProfil = DaoProfil.findById(idSigner);
		String passCert = myProfil.getCertifpasswd();
		char[] password = passCert.toCharArray();
		byte[] certificat = myProfil.getCertif();

		// CHARGEMENT DU FICHIER PKCS#12
		KeyStore ks = null;
		Security.addProvider(new BouncyCastleProvider());
		try {
			ks = KeyStore.getInstance("PKCS12");
			ks.load(new ByteArrayInputStream(certificat), password);
		} catch (Exception e) {
			System.err
					.println("Erreur: le certificat n'est pas pkcs#12 valide ou passphrase incorrect");
		}

		// RECUPERATION DU COUPLE CLE PRIVEE/PUBLIQUE ET DU CERTIFICAT PUBLIQUE

		X509Certificate cert = null;
		PrivateKey privatekey = null;

		try {
			Enumeration en = ks.aliases();
			String ALIAS = "";
			Vector vectaliases = new Vector();

			while (en.hasMoreElements())
				vectaliases.add(en.nextElement());
			String[] aliases = (String[]) (vectaliases.toArray(new String[0]));
			for (int i = 0; i < aliases.length; i++)
				if (ks.isKeyEntry(aliases[i])) {
					ALIAS = aliases[i];
					break;
				}
			privatekey = (PrivateKey) ks.getKey(ALIAS, password);
			cert = (X509Certificate) ks.getCertificate(ALIAS);

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			// Chargement du fichier qui va �tre sign�

			File fileToSign = new File(repPath + "//" + myFileName);
			byte[] buffer = new byte[(int) fileToSign.length()];
			DataInputStream in = new DataInputStream(new FileInputStream(
					fileToSign));
			in.readFully(buffer);
			in.close();

			// Chargement des certificats qui seront stock�s dans le fichier .p7
			// Ici, seulement le certificat markSmith.p12 sera associ�.
			// Par contre, la cha�ne des certificats non.

			List certList = new ArrayList();
			certList.add(cert);

			Store certs = new JcaCertStore(certList);
			// CMSSignedDataGenerator signGen = new CMSSignedDataGenerator();

			// privatekey correspond � notre cl� priv�e r�cup�r�e du fichier
			// PKCS#12
			// cert correspond au certificat publique markSmith.p12
			// Le dernier argument est l'algorithme de hachage qui sera utilis�
			ContentSigner sigGen = new JcaContentSignerBuilder("SHA1withRSA")
					.setProvider("BC").build(privatekey);
			X509CertificateHolder sigCert = new JcaX509CertificateHolder(cert);

			CMSSignedDataGenerator gen = new CMSSignedDataGenerator();

			gen.addSignerInfoGenerator(new SignerInfoGeneratorBuilder(
					new BcDigestCalculatorProvider()).build(sigGen, sigCert));
			gen.addCertificates(certs);

			CMSTypedData msg = new CMSProcessableByteArray(buffer);
			// Generation du fichier CMS/PKCS#7
			// L'argument deux permet de signifier si le document doit �tre
			// attach� avec la signature
			// Valeur true: le fichier est attach� (c'est le cas ici)
			// Valeur false: le fichier est d�tach�

			CMSSignedData sigData = gen.generate(msg);
			byte[] signeddata = sigData.getEncoded();
			// Ecriture du buffer dans un fichier.
			System.out.println("----signature termin�e----");
			System.out.println(myFileName);
			String SignaturePath = repPath + "//signature"
					+ myFileName.split("\\.")[0].trim() + ".pk7";
			FileOutputStream envfos = new FileOutputStream(SignaturePath);
			envfos.write(signeddata);
			envfos.close();
			System.out.println(SignaturePath);
			File_ file = new File_();
			file = myFile;
			file.setSigned(1);
			DaoFile.updatefile(myFile, file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void PreSignature(String myFileName, String repPath) {

		if (myFileName.endsWith(".pdf")) {
			System.out.println("fichier PDF");
			try {
				PdfReader pdfReader = new PdfReader(repPath + "//" + myFileName);

				PdfStamper pdfStamper = new PdfStamper(pdfReader,
						new FileOutputStream(repPath + "//TMP" + myFileName));

				Image image = Image
						.getInstance("ressources/signatureIcone.jpg");

				for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {

					PdfContentByte content = pdfStamper.getUnderContent(i);

					image.setAbsolutePosition(10, 10);
					image.scalePercent(33f);

					content.addImage(image);
				}

				pdfStamper.close();

				// renommer le fichier temporaire pour n'avoir qu'un fichier
				// avec l'etiquette et de meme nom
				File f = new File(repPath + "//" + myFileName);
				f.delete();
				new File(repPath + "//TMP" + myFileName).renameTo(f);

			} catch (IOException e) {
				e.printStackTrace();
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		} else
			System.out.println("autre fichier");
	}

}