package signature;

import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Calendar;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfPKCS7;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfSignatureAppearance;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;

public class VerificationPDF {
	
	/**
	 * Nom du document PDF g�n�r� non sign�
	 */
	static String fname  = "C:\\Users\\phenix\\Desktop\\signature\\HelloWorld.pdf" ;
 
	/**
	 * Nom du document PDF g�n�r� sign�s
	 */
	static String fnameS = "C:\\Users\\phenix\\Desktop\\signature\\HelloWorld_sign.pdf" ;
	static String fnameR = "C:\\Users\\phenix\\Desktop\\signature\\HelloWorld" ;
	
	public static final boolean verifyPDF()
				throws IOException, DocumentException, Exception
				{
			KeyStore kall = PdfPKCS7.loadCacertsKeyStore();
	
			PdfReader reader = new PdfReader(fnameS);
			AcroFields af = reader.getAcroFields();
	
			// Recherche de l'ensemble des signatures
			ArrayList names = af.getSignatureNames();
	
			// Pour chaqu'une des signatures
			for (int k = 0; k < names.size(); ++k) {
				String name = (String)names.get(k);
				// Affichage du nom
				System.out.println("Signature name: " + name);
				PdfDictionary sig = af.getSignatureDictionary(name);
				System.out.println(sig.getAsString(PdfName.REASON));
				System.out.println("Signature covers whole document: "
						+ af.signatureCoversWholeDocument(name));
				// Affichage sur les revision - version
				System.out.println("Document revision: " + af.getRevision(name) + " of "
						+ af.getTotalRevisions());
				// Debut de l'extraction de la "revision"
				FileOutputStream out = new FileOutputStream(fnameR +"_revision_"
						+ af.getRevision(name) + ".pdf");
				byte bb[] = new byte[8192];
				InputStream ip = af.extractRevision(name);
				int n = 0;
				while ((n = ip.read(bb)) > 0) out.write(bb, 0, n);
				out.close();
				ip.close();
				// Fin extraction revision
	
				PdfPKCS7 pk = af.verifySignature(name);
				Calendar cal = pk.getSignDate();
				Certificate pkc[] = pk.getCertificates();
				// Information sur le certificat, le signataire
				System.out.println("Subject: "
						+ PdfPKCS7.getSubjectFields(pk.getSigningCertificate()));
				// Le document � t'il ete modifi� ?
				System.out.println("Document modified: " + !pk.verify());
	
				// Le certificat est il valide ? Attention on recherche la chaine de certificats
				Object fails[] = PdfPKCS7.verifyCertificates(pkc, kall, null, cal);
				if (fails == null)
					System.out.println("Certificates verified against the KeyStore");
				else
					System.out.println("Certificate failed: " + fails[1]);
			}
			return true ;
				}
}

